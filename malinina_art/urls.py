# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from malinina_art import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'malinina_art.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # Home app urls
    url(r'^$', include('home.urls', namespace='home'), name='home'),
    url(r'^about-us$', include('about.urls', namespace='about'), name='about'),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT }),
)
urlpatterns += staticfiles_urlpatterns()
