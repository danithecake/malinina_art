from django.shortcuts import render, render_to_response
from django.template import RequestContext
from malinina_art import settings


def home(request, *args):
    """
    Render home page
    """
    return render_to_response('home/index.html', {
        'base_dir': settings.BASE_DIR
    }, RequestContext(request))
