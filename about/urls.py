# -*- coding: utf-8 -*-
__author__ = 'danil'
from django.conf.urls import patterns, url
from about import views


urlpatterns = patterns('',
    # Launch main page controller
    url(r'^$', views.index, name='about'),
)
