from django.shortcuts import render, render_to_response
from django.template import RequestContext

def index(request, *args):
    """
    Render about page
    """
    return render_to_response('about/index.html', {}, RequestContext(request))
